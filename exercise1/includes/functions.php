<?php

class Functions {

    /*
     * Validate balanced brackets
     */
    public static function isBalance($string) {

        $map_brackets = [ '[' => ']', '{' => '}', '(' => ')' ]; // An array of key/value in order/normal.

        $map_brackets_inverted = array_flip($map_brackets); // An array of key/value pairs to be inverted.

        $stack_brackets = [];
        $c = 0;

        for ($i = 0; $i < mb_strlen($string); $i++) {

            $char = trim( $string[$i] ); // Get character of position and use trim() for remove spaces.

            if ( isset($map_brackets[$char]) ) {
                $c++;
                $stack_brackets[] = $map_brackets[$char];

            } else if ( isset($map_brackets_inverted[$char]) ) {
                $c++;
                $expected = array_pop($stack_brackets); // Returns the last value of array.

                if ( ($expected === NULL) || ($char != $expected) ) {
                    return false;
                }
            }
        }

        return ($c == 0) ? false : empty($stack_brackets); // return true or false.
    }

    public static function escape($str) {
        return filter_var(htmlspecialchars(trim($str), ENT_QUOTES), FILTER_SANITIZE_STRING);
    }


}