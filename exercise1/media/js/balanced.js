$(function(){

    function clearAlert() {
        setTimeout(function(){
            $(".alert").html('').removeClass().addClass('alert');
        }, 3000);
    }

    $("#form-string").submit(function( event ) {
        $.ajax({
            method: "POST",
            url: "ajax/brackets.validate.php",
            data: $(this).serialize()
        })
        .done(function( data ) {
            $(".alert").addClass(data.class).html(data.message);
            clearAlert();
        });
        event.preventDefault();
    });

});