<!DOCTYPE html>
<html lang="en">
    <meta charset="utf-8">
    <title> Bravi </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Daniel Kreutz" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit" content="1 week" />
    <meta name="distribution" content="Global" />

    <link rel="icon" href="media/img/favicon.ico">
    <link rel="stylesheet" href="media/css/bootstrap.min.css">
    <link rel="stylesheet" href="media/css/balanced.css">

    <script src="media/js/jquery-3.2.1.min.js"></script>
<body>

    <div class="container-fluid">

        <div class="page-header">
            <h1> Balanced Brackets </h1>
        </div>

        <div class="col-sm-12 info">
            Function that takes a string of brackets as the input, and determines if the order of
            the brackets is valid. <br/>
            A bracket is considered to be any one of the following characters: (, ), {, }, [, or ].
            <br/>
            Examples: <br/>
            ● (){}[] is valid <br/>
            ● [{()}](){} is valid <br/>
            ● []{() is not valid <br/>
            ● [{)] is not valid <br/>
            ● [9] * 2 - ( 1 - 3 * [8-15]) + (89-5) is valid <br/>
            ● ( 1 * 4 [ 9 ) - 1] + (4*3) is not valid <br/>
        </div>

        <div class="login">

            <h3> Write your string: </h3>

            <form id="form-string" method="post" action="" class="">
                <input type="text" class="string" name="string" placeholder="" required="required" />
                <button type="submit" class="btn btn-primary btn-block btn-large"> Verify </button>
            </form>

            <br/>

            <div class="alert" role="alert"></div>
        </div>

    </div>

    <script src="media/js/bootstrap.min.js"></script>
    <script src="media/js/balanced.js"></script>

</body>
</html>