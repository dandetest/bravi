<?php

header('Content-Type: application/json');

include_once '../includes/functions.php';

if( isset($_POST['string']) ){

 $string = Functions::escape($_POST['string']);

 $response = (Functions::isBalance($string))
             ? ['class' => 'alert-success', 'message' => 'Your string is valid! :) ']
             : ['class' => 'alert-danger',  'message' => 'Your string is not valid! :('];

 echo json_encode( $response );

}