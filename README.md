=======================================================
=== REQUIREMENTS
=======================================================
- Server Apache or similar
- PHP version 7+
- MySQL version 5.6+

# Repository
https://bitbucket.org/dandetest/bravi.git
Password: 10201030
E-mail: bravi_test@datadk.com.br

Copy project 'bravi' for directory public (www or public_html)
Configure your server 'localhost'

=======================================================
=== EXERCISE 1
=======================================================
### TEST
- Open Browser
- Open URL: http://localhost/bravi/exercise1

=======================================================
=== EXERCISE 2
=======================================================
### SETUP

**1: Create database MySQL**
- Example of database name: exercise2

**2: Update MySQL database connection data**
- Edit file: /bravi/exercise2/application/config/database.php
- Update lines 78 through 81 with connection data

**3: Run the migration to update the database**
- Open Browser
- Open URL: http://localhost/bravi/exercise2/migrate

=======================================================
### TEST

To test the API you need software.
For browser Google Chrome the software Postman: 
https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en
or you can use other similar software.
--------------------------
# 1: Open the software Postman.

# Create Person
- Choose method: POST
- Insert URL: http://localhost/bravi/exercise2/api/person
- Insert the fields:
                    name : Maria
                    email: maria@gmail.com
                    birthday: 1980-02-15
- Click on button: SEND
- A reply must be TRUE for Insertion with success 

# Update Person
- For record update, insert the field 'id', for example:
        id : 1
        name : Maria Oliveira
        email: maria_oliveira@gmail.com
        birthday: 1980-02-15  
- Click on button: SEND
- A reply must be TRUE for Insertion with success 

# Read All Persons
- Choose method: GET
- Insert URL: http://localhost/bravi/exercise2/api/person
- Click on button: SEND
- A reply with list of all persons

# Read One Person
- Choose method: GET
- Insert URL: http://localhost/bravi/exercise2/api/person
- For record read, insert the field 'id' on URL, for example: id: 1
  The new URL is: http://localhost/bravi/exercise2/api/person/id/1
- Click on button: SEND
- If the record with ID = 1 exists, the response will be the data of the Person                                            

# Delete Person
- Choose method: DELETE
- Insert URL: http://localhost/bravi/exercise2/api/person
- For record delete, insert the field 'id' on URL, for example: id: 1
  The new URL is: http://localhost/bravi/exercise2/api/person/id/1
- Click on button: SEND
- If the record is deleted, the response is True                                          

--------------------------

# Create Contact
- Choose method: POST
- Insert URL: http://localhost/bravi/exercise2/api/contact
- Insert the fields:
                    type: phone
                    desribe: +353 833024642                    
                    person_id: 1
- Click on button: SEND
- A reply must be TRUE for Insertion with success 

# Update Contact
- Choose method: POST
- Insert URL: http://localhost/bravi/exercise2/api/contact
- For record update, insert the field 'id', for example:
                    type: phone
                    desribe: +353 88334400                   
                    id: 1
- Click on button: SEND
- A reply must be TRUE for Insertion with success 

# Read Contact
- Choose method: GET
- Insert URL: http://localhost/bravi/exercise2/api/contact
- For record read, insert the field 'person_id' on URL, for example: person_id: 1
  The new URL is: http://localhost/bravi/exercise2/api/contact/person_id/1
- Click on button: SEND
- If the records with PERSON_ID = 1 exists, the response will be the data of the Contacts                                            

# Delete Contact
- Choose method: DELETE
- Insert URL: http://localhost/bravi/exercise2/api/contact
- For record delete, insert the field 'id' on URL, for example: id: 1
  The new URL is: http://localhost/bravi/exercise2/api/contact/id/1
- Click on button: SEND
- If the record is deleted, the response is True                                          

=======================================================
=======================================================
=== REQUIREMENTS FOR EXERCISE 3
=======================================================
- PHP version 7.2 or greater.
- MySQL version 5.6+
- Folder files: /bravi/exercise3/Folder_Files_Weather_Widget
=======================================================
=== EXERCISE 3
=======================================================
### SETUP

**1: Create database MySQL**
- Example of database name: exercise3
- Update database with SQL: /bravi/exercise3/Folder Files Weather Widget/exercise3.sql
or install new aplication WordPress.
If you use the file SQL (exercise3.sql), the access on panel admin (http://localhost/bravi/exercise3/wp-admin) is:
Login/User: bravi
Password: 10201030

**2: Install Widget WeatherDK**
- Access the folder: /bravi/exercise3/Folder_Files_Weather_Widget/
- Copy the files:
"weather_widget.php" **to** /bravi/exercise3/wp-content/themes/**YOUR_THEME**/
"weatherdk.js" **to** /bravi/exercise3/wp-content/themes/**YOUR_THEME**/assets/js/
"weatherdk.css" **to** /bravi/exercise3/wp-content/themes/**YOUR_THEME**/assets/css/

- After copy/paste the files, add to file: /bravi/exercise3/wp-content/themes/twentyseventeen/functions.php
this code: include ('weather_widget.php');  

**3: Active widget WeatherDK**
- Access the panel admin: http://localhost/bravi/exercise3/wp-admin
Input your login and password
- Access on Menu: Appearance > Widgets
or URL: http://localhost/bravi/exercise3/wp-admin/widgets.php
- Should display the widget WeatherDK


- In the file "weatherdk.js" you can change API Key and others data. 
    