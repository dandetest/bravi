<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends Base_model{

    function __construct(){
        parent::__construct();
        $this->table = 'contact';
    }

    public function GetContacts($personId=null, $column_sort='id', $order = 'asc'){
        if(is_null($personId))
            return false;

        $this->db->order_by($column_sort, $order);
        $this->db->where('person_id', $personId);
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

}
