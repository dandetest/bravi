<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_model extends CI_Model {

    /*
     * Name of table
     */
    var $table = "";

    function __construct() {
        parent::__construct();
    }

    /**
     * Insert record
     *
     * @param array $data
     *
     * @return boolean
     */
    function Insert($data) {
        if(!isset($data))
            return false;

        return $this->db->insert($this->table, $data);
    }

    /**
     * Get register
     *
     * @param integer $id ID of record
     *
     * @return array
     */
    function GetById($id) {
        if(is_null($id))
            return false;

        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return null;
        }
    }

    /**
     * All records
     *
     * @param string $sort column for order
     *
     * @param string $order Type: ASC ou DESC
     *
     * @return array
     */
    function GetAll($sort = 'id', $order = 'asc') {
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

    /**
     * Update record
     *
     * @param integer $int ID of record
     *
     * @param array $data
     *
     * @return boolean
     */
    function Update($id, $data) {
        if(is_null($id) || !isset($data))
            return false;

        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete record
     *
     * @param integer $int ID
     *
     * @return boolean
     */
    function Delete($id) {
        if(is_null($id))
            return false;

        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }

}