<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_contact extends CI_Migration {

    /*
    * Name of table
    */
    var $table = "contact";

    public function up(){

        $this->dbforge->add_field(array(
            'ID' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE,
                'auto_increment' => TRUE
            ),
            'PERSON_ID' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'TYPE' => array(
                'type' => 'ENUM("phone","email","whatsapp")',
                'default' => 'phone',
                'null' => FALSE
            ),
            'DESCRIBE' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => FALSE
            )
        ));

        $this->dbforge->add_key('ID', TRUE);

        $attributes = array('ENGINE' => 'InnoDB', 'CHARSET' => 'utf8');

        $this->dbforge->create_table($this->table, FALSE, $attributes);

        $nameFK1 = 'fk_contact_person';
        $this->db->query('ALTER TABLE '.$this->table.' ADD CONSTRAINT '.$nameFK1.' FOREIGN KEY (PERSON_ID) REFERENCES person(ID) ON DELETE CASCADE ON UPDATE NO ACTION');

    }


    public function down(){
        $this->dbforge->drop_table($this->table);
    }
}