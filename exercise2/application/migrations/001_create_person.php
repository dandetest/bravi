<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_person extends CI_Migration {

    /*
    * Name of table
    */
    var $table = "person";

    public function up(){

        $this->dbforge->add_field(array(
            'ID' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE,
                'auto_increment' => TRUE
            ),
            'NAME' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => FALSE,
            ),
            'EMAIL' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE,
            ),
            'BIRTHDAY' => array(
                'type' => 'DATE',
                'null' => TRUE,
            ),
        ));

        $this->dbforge->add_key('ID', TRUE);

        $attributes = array('ENGINE' => 'InnoDB', 'CHARSET' => 'utf8');

        $this->dbforge->create_table($this->table, FALSE, $attributes);
    }


    public function down(){
        $this->dbforge->drop_table($this->table);
    }
}