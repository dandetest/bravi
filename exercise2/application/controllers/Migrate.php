<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    /*
     * Up all migrations
     */
    public function index() {

        $this->load->library('migration');

        /*
         * Execute all migrations
        */
        foreach ($this->migration->find_migrations() as $version => $file){

            if(  $this->migration->version($version) ){
                echo "Migration ".$version." success! File: ".$file." <br/>";
            }else{
                echo $this->migration->error_string();
            }

        }
    }

    /*
     * Down migration
     */
    public function down(){
        $this->load->library('migration');

        $version = 0;

        if(  $this->migration->version($version) ){
            echo "Migration down for version ".$version." success! <br/>";
        }else{
            echo $this->migration->error_string();
        }
    }

}