<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Address: /api/person/PARAMS/format/(xml,json,csv,html)
 * Default format: JSON
 */

/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Api extends REST_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("Person_model", "person");
        $this->load->model("Contact_model", "contact");
    }

    /*
     * Read all or one record of table
     * @param integer $id ID of record
     */
    public function person_get(){

        $id = $this->get('id');
        $column_sort = $this->get('sort');

        $data = ($id == NULL) ? $this->person->GetAll($column_sort) : $this->person->GetById($id);

        if($data != NULL){
            $this->response($data, REST_Controller::HTTP_OK);
        }else{
            $this->set_response(['status' => FALSE], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    /*
     * Method 'post' use for Create or Update record
     * @param integer $id ID of record
     */
    public function person_post(){
        $id = $this->post('id');

        $data = [
                 'NAME'      => $this->post('name'),
                 'EMAIL'     => $this->post('email'),
                 'BIRTHDAY'  => $this->post('birthday')
                ];

        $status = ($id == NULL) ? $this->person->Insert($data) : $this->person->Update($id, $data);

        $this->set_response(['status' => $status], REST_Controller::HTTP_CREATED);
    }

    /*
     * Delete record Person
     * Use arg 'id' on URL for example: /api/person/id/DB_ID
     * @param integer $id ID of record
     */
    public function person_delete(){
        $id = $this->get('id');
        $status = $this->person->Delete($id);
        $this->set_response(['status' => $status], REST_Controller::HTTP_OK);
    }


    /* ================ CONTACTS ================ */

    /*
     * Read all or one record of table
     * @param integer $id ID of record
     * @param integer $person_id ID of record Person
     * @param string $column_sort name of collumn tablet Contact
     */
    public function contact_get(){

        $personId    = $this->get('person_id');
        $id          = $this->get('id');
        $column_sort = $this->get('sort');

        if( $personId != NULL ){
            $data = $this->contact->GetContacts($personId, $column_sort);
        }else{
            $data = ($id == NULL) ? $this->contact->GetAll($column_sort) : $this->contact->GetById($id);
        }

        if($data != NULL){
            $this->response($data, REST_Controller::HTTP_OK);
        }else{
            $this->set_response(['status' => FALSE], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    /*
     * Method 'post' use for Create or Update record
     * @param integer $person_id ID of record Person
     * @param integer $id ID of record Contact
     */
    public function contact_post(){
        $personId = $this->post('person_id');
        $id       = $this->post('id');

        if( $personId == null && $id == null ){
            $this->set_response(['status' => FALSE], REST_Controller::HTTP_NOT_FOUND);

        }else{
            $data = [
                'TYPE' => $this->post('type'),
                'DESCRIBE' => $this->post('describe')
            ];

            if( $personId != null ){
                $data['PERSON_ID'] = $personId;
            }

            $status = ($id == NULL) ? $this->contact->Insert($data) : $this->contact->Update($id, $data);

            $this->set_response(['status' => $status], REST_Controller::HTTP_CREATED);
        }
    }

    /*
   * Delete record Contact
   * Use arg 'id' on URL for example: /api/contact/id/DB_ID
   */
    public function contact_delete(){
        $id = $this->get('id');
        $status = $this->contact->Delete($id);
        $this->set_response(['status' => $status], REST_Controller::HTTP_OK);
    }

}