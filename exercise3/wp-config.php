<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'exercise3');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'iCCzZE&3%w^]s.I|BlK1=yjy]o*$uu[F&=w |eOG*B=Q}|  J8{Q:J()0<G(p]P~');
define('SECURE_AUTH_KEY',  'W}%bdc8To>|;,wz!hi86z(jlJQ^Lp(P?aBf:3S]5?PSomOC5[[0= i.M2}kHEd2V');
define('LOGGED_IN_KEY',    'dsD>J~4)^B_ygr4+g}Bp~_;CLC6=n2Vc$~N^js,Fj^2`2nnvNuZtG>V$um-T]Po;');
define('NONCE_KEY',        ']6_QTwyk>W^|q&*p//vz,`OV$f[yo#f3-D?V.b3yHPAN<N{=^)cyb[jn4eo3=Iuj');
define('AUTH_SALT',        'vVM:Q,x;m-^jS_*Et*I+KZR5]j?hX3C,hOPE@/<eR7PmxwgIqiXYHo;YGB)hzy=R');
define('SECURE_AUTH_SALT', 'mf_coyg1s|%K9>tdZxcp@1z}jGu(T;-e-m^Y#6$z,IPHz~lxm%/xaEg%E(ha_pV(');
define('LOGGED_IN_SALT',   'Hy>m.9<cN%vYk8q*Kr%NqKGhY7GKZ7-<oBGQ!$D?`<L,|uRrG.C,*I?HR,[!/c}U');
define('NONCE_SALT',       'wr3iso#bq`b7WJb!k!/}Tc^rDk!h;_J4p=lx9m~4Er-_XDr:?zwLsh3+ja=~-o2R');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
