<?php

class weather_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
// widget ID
            'weather_widget',
// widget name
            __('Weather DK', ' weather_widget_domain'),
// widget description
            array( 'description' => __( 'Weather DK with openweathermap.org', 'weather_widget_domain' ), )
        );
    }

    // Show on site
    public function widget( $args, $instance ) {
        $title      = apply_filters( 'widget_title', $instance['title'] );
        $text_input = apply_filters( 'widget_text_input', $instance['text_input'] );
        $api_key    = apply_filters( 'widget_api_key', $instance['api_key'] );

        echo $args['before_widget'];

        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];
//output
        ?>

        <form method="get" class="form weatherdk-form" action="">
            <p>
                <input class="" id="input-weather-city" name="" type="text" value="" placeholder="<?= $text_input; ?>" />
                <button type="button" class="button-weather-city"> Search </button>
            </p>
        </form>

        <div id="openweathermap-widget-15" style="display:none" class="container-widget container-widget--15">
            <div id="container-openweathermap-widget-15">
                <link href="//openweathermap.org/themes/openweathermap/assets/vendor/owm/css/openweathermap-widget-right.min.css" rel="stylesheet">
                <div class="widget-right weather-right--type1 widget-right--brown">

                    <div class="widget-right__header widget-right__header--brown">
                        <div class="widget-right__layout">
                            <div>
                                <h2 class="widget-right__title"> </h2>
                                <p class="widget-right__description"> </p>
                            </div>
                        </div>
                        <img src="//openweathermap.org/themes/openweathermap/assets/vendor/owm/img/widgets/04d.png" width="128" height="128" alt="Weather in London, GB" class="weather-right__icon weather-right__icon--type1">
                    </div>

                    <div class="weather-right weather-right--brown">
                        <div class="weather-right__layout">
                            <div class="weather-right__temperature"> </div>
                            <div class="weather-right__weather">
                                <div class="weather-right-card">
                                    <table class="weather-right__table">
                                        <tbody><tr class="weather-right__items">
                                            <th colspan="2" class="weather-right__item">Details</th>
                                        </tr>
                                        <tr class="weather-right__items">
                                            <td class="weather-right__item">Temp. Máx.</td>
                                            <td class="weather-right__item weather-right__temp-max"> </td>
                                        </tr>
                                        <tr class="weather-right__items">
                                            <td class="weather-right__item">Wind</td>
                                            <td class="weather-right__item weather-right__wind-speed"> </td>
                                        </tr>
                                        <tr class="weather-right-card__items">
                                            <td class="weather-right__item">Humidity</td>
                                            <td class="weather-right__item weather-right__humidity"> </td>
                                        </tr>
                                        <tr class="weather-right-card__items">
                                            <td class="weather-right__item">Pressure</td>
                                            <td class="weather-right__item weather-right__pressure"> </td>
                                        </tr>
                                        <tr class="weather-right-card__items">
                                            <td class="weather-right__item"> Latitude </td>
                                            <td class="weather-right__item weather-right__lat"></td>
                                        </tr>
                                        <tr class="weather-right-card__items">
                                            <td class="weather-right__item">Longitude</td>
                                            <td class="weather-right__item weather-right__long"></td>
                                        </tr>
                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div></div>

        <?php
      echo $args['after_widget'];
    }

    // Form on panel
    public function form( $instance ) {
        $api_key    = isset( $instance[ 'api_key' ] )     ? $instance[ 'api_key' ]    : '';
        $text_input = isset( $instance[ 'text_input' ] )  ? $instance[ 'text_input' ]    : '';
        $title      = isset($instance[ 'title' ])         ? $instance[ 'title' ]      : __( 'Default Title', 'weather_widget_domain' );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
            <br/><br/>
            <label for="<?php echo $this->get_field_id( 'text_input' ); ?>"><?php _e( 'Default field text' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'text_input' ); ?>" name="<?php echo $this->get_field_name( 'text_input' ); ?>" type="text" value="<?php echo esc_attr( $text_input ); ?>" />

            <?php /*    <br/><br/>
                <label for="<?php echo $this->get_field_id( 'api_key' ); ?>"><?php _e( 'API Key (openweathermap.org):' ); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id( 'api_key' ); ?>" name="<?php echo $this->get_field_name( 'api_key' ); ?>" type="text" value="<?php echo esc_attr( $api_key ); ?>" />
            */ ?>
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title']        = ( ! empty( $new_instance['title'] ) )       ? strip_tags( $new_instance['title'] ) : '';
        $instance['text_input']   = ( ! empty( $new_instance['text_input'] ) )  ? strip_tags( $new_instance['text_input'] ) : '';
        $instance['api_key']      = ( ! empty( $new_instance['api_key'] ) )     ? strip_tags( $new_instance['api_key'] ) : '';
        return $instance;
    }

}

function weather_scripts() {
    wp_enqueue_style('WeatherDKCSS', get_template_directory_uri() . '/assets/css/weatherdk.css', array(), '1.0', false );
    wp_enqueue_script( 'weatherDKJS', get_template_directory_uri() . '/assets/js/weatherdk.js', array(), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'weather_scripts' );

function weather_register_widget() {
    register_widget( 'weather_widget' );
}
add_action( 'widgets_init', 'weather_register_widget' );