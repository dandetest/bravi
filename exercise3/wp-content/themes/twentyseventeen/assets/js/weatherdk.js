(function( $ ) {
    console.log('Loaded weatherdk.js');

    var api_key        = "9bed68493d70493e41254f25ad1249bd"; // Api key of openweathermap.org
    var city_default   = "Dublin,IE";
    var url            = "//api.openweathermap.org/data/2.5/weather?mode=json&units=metric&appid="+api_key+"&q=";
    var url_img        = "//openweathermap.org/themes/openweathermap/assets/vendor/owm/img/widgets/";

    function getWeather(apiKey, city){
        jQuery.getJSON( url + city)
            .done(function(json) {
                if(!json){
                    jQuery("#openweathermap-widget-15").slideUp();
                }else{
                    var temp = parseInt(json.main.temp.toFixed(2).slice(0,2));
                    var temp_max = parseInt(json.main.temp_max.toFixed(2).slice(0,2));

                    jQuery(".widget-right__title").html( json.name + ', '+ json.sys.country );
                    jQuery(".widget-right__description").html( json.weather[0].description );
                    jQuery(".weather-right__icon").attr("src", url_img+""+json.weather[0].icon+".png");
                    jQuery(".weather-right__temperature").html( temp + "°C");
                    jQuery(".weather-right__temp-max").html( temp_max + "°C" );
                    jQuery(".weather-right__long").html(json.coord.lon );
                    jQuery(".weather-right__lat").html(json.coord.lat );
                    jQuery(".weather-right__wind-speed").html( json.wind.speed + " m/s");
                    jQuery(".weather-right__humidity").html( json.main.humidity + " %" );
                    jQuery(".weather-right__pressure").html( parseInt(json.main.pressure) + " hPa" );
                    jQuery("#openweathermap-widget-15").slideDown();
                    console.log(json);
                }
            })
            .fail(function( jqxhr, textStatus, error ) {
                jQuery("#openweathermap-widget-15").hide();
                alert('City not found!');
                console.log(error);
            });
    }

    // Action button Search
    jQuery(".button-weather-city").click(function(){
       var city = jQuery("#input-weather-city").val();
       if(city !== ""){
          getWeather(api_key, city);
       }
    });

    // Action when press button Enter on input text
    $('#input-weather-city').keypress(function(ev){
        if (ev.which === 13){
            jQuery(".button-weather-city").click();
            return false;
        }
    });

    // Show default city
    getWeather(api_key, city_default);

})( jQuery );